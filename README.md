# Movie Trailer Website
This project includes a server-side python application to store a list of movies, including the following features:
  - Show index about listed movies
  - Display movie box art imagery
  - Show movie name and trailer links

### Tech

* [Python](https://www.python.org/) - The magic programming language.
* [HTML](https://www.w3schools.com/html/) - The language to make the website
* [CSS](https://www.w3schools.com/css/) - The language to make your website looks good
* [Twitter Bootstrap](https://getbootstrap.com/) - great UI boilerplate for modern web apps

### Installation

This project requires [Python](https://www.python.org/) v2.7+ to run.

Unzip the file and simply run(This command will also build the webpage and run the program):

```sh
$ python entertainment_center.py
```

### Todos

 - Integrate with some movie website apis
 - Add more interactive functionalities
