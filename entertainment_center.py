import media
import fresh_tomatoes

"""Inits multiple movie instance objects based on fresh_tomatoes framework"""
toy_story = media.Movie("Toy Story",
                        "A story of a boy and his toys that comes to life",
                        "http://upload.wikimedia.org/wikipedia/en/1/13/Toy_Story.jpg",
                        "https://www.youtube.com/watch?v=vwyZH85NQC4"
                        )
avatar = media.Movie("Avatar",
                     "A story of marine on alien planet",
                     "https://upload.wikimedia.org/wikipedia/en/b/b0/Avatar-Teaser-Poster.jpg",
                     "https://www.youtube.com/watch?v=uZNHIU3uHT4"
                     )
fight_club = media.Movie("Fight Club",
                         "A story of a man fighting with himself",
                         "https://upload.wikimedia.org/wikipedia/en/f/fc/Fight_Club_poster.jpg",
                         "https://www.youtube.com/watch?v=SUXWAEX2jlg"
                         )
batman = media.Movie("Batman",
                     "A famous superhero movie",
                     "https://upload.wikimedia.org/wikipedia/en/8/8a/Dark_Knight.jpg",
                     "https://www.youtube.com/watch?v=kmJLuwP3MbY"
                     )
inception = media.Movie("Inception",
                        "It is a long story about dream inception",
                        "https://upload.wikimedia.org/wikipedia/en/2/2e/Inception_%282010%29_theatrical_poster.jpg",
                        "https://www.youtube.com/watch?v=66TuSJo4dZM"
                        )
social_network = media.Movie("Social Network",
                             "It is the story about Facebook",
                             "https://upload.wikimedia.org/wikipedia/en/7/7a/Social_network_film_poster.jpg",
                             "https://www.youtube.com/watch?v=lB95KLmpLR4"
                             )

"""Assign movie objects into an array, so we can use it in the framework"""
movies = [toy_story, avatar, fight_club, batman, inception, social_network]

"""Init the page by calling the method in the framework"""
fresh_tomatoes.open_movies_page(movies)
