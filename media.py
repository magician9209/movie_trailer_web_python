import webbrowser


class Movie():
    """Movie is a class to generate movie instance, including movie info

    Attributes:
        title: A string indicating name of the movie instance
        storyline: A string indicating movie instance storyline
        poster_image_url: A string indicating movie instance poster image url
        trailer_youtube_url: A string indicating movie trailer link on youtube
    """

    def __init__(self,
                 movie_title,
                 movie_storyline,
                 poster_image,
                 trailer_youtube):
        """Inits Movie with arguments"""
        self.title = movie_title
        self.storyline = movie_storyline
        self.poster_image_url = poster_image
        self.trailer_youtube_url = trailer_youtube

    def show_trailer(self):
        """Performs operation openning your web browser
           and start playing trailer
        """
        webbrowser.open(self.trailer_youtube_url)
